﻿// TP_2024.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme
// commence et se termine à cet endroit.
//
using namespace std;

#include <iomanip>
#include <iostream>
#include <string>
#include <windows.h>  // Pour la fonction sleep()

void ex1a() {
    int a, b, tmp;
    a = 3;
    b = 5;
    cout << "Valeurs avant inversion : a=" << a << ", b=" << b << endl;
    tmp = b;
    b = a;
    a = tmp;
    cout << "Valeurs apres inversion : a=" << a << ", b=" << b << endl;
}

void ex1b() { 
    /*
    useless
    */
}

void ex1c() { int saisie;
    cout << "Veuillez saisir un entier : ";
    cin >> saisie;
    cout << "Valeur saisie : " << saisie << endl;
}

void ex1d1() {
    /////////////////////////////////////////////////
    // Exercice I.D.1 - Catégories d'âge
    /*
    -6 à 7 ans : poussin
    - 8 à 9 ans : pupille
    - 10 à 11 ans : minime
    - 12 ans et plus : cadet
 */

    int age;
    cout << "Quel est votre age ? ";
    cin >> age;

    // Si l'âge est compris entre 6 et 7 inclus alors...
    if (age >= 6 && age <= 7) {
        // afficher la chaîne ci-dessous
        cout << "tu es poussin" << endl;
    }
    if (age >= 8 && age <= 9) {
        cout << "tu es pupille" << endl;
    }
    if (age >= 10 && age <= 11) {
        cout << "tu es minime" << endl;
    }
    if (age >= 12) cout << "tu es cadet" << endl;
}

void ex1d2() {
    /////////////////////////////////////////////////
    // Exercice I.D.2 - les conditions
    // Note : années bissextiles non prises en charge
    // donner le lendemain du jour saisi
    int jour, mois, an;
    int ljour, lmois, lan;

    cout << "Saisir le jour le mois l'annee svp (separes par des espaces ou "
            "RETOUR CHARIOT) ";  // carriage return
    cin >> jour >> mois >> an;
    cout << "Le lendemain du " << jour << " " << mois << " " << an << endl;
    // on passe au lendemain
    ljour = jour + 1;

    lmois = mois;
    lan = an;

    // fin du mois
    // mois 31 jours - 01 03 05 07 08 10 12
    // mois 30 jours - 04 06 09 11
    // février - 28
    // cas fin mois 31
    if ((ljour == 32) && (mois == 1 || mois == 3 || mois == 5 || mois == 7 ||
                          mois == 8 || mois == 10 || mois == 12)) {
        ljour = 1;
        // % : modulo
        // ça remet le mois à 1 => quand la saisie correspond au 31/12 le mois
        // passe à Janvier
        lmois = (lmois + 1) % 12;
        if (lmois == 1) lan++;  // cas 31 déc
    }
    // cas fin mois 30
    if ((ljour == 31) && (mois == 4 || mois == 6 || mois == 9 || mois == 11)) {
        ljour = 1;
        lmois++;
    }

    // cas février
    if ((ljour == 29) && (lmois == 2)) {
        ljour = 1;
        lmois++;
    }

    cout << setw(2) << setfill('0') << ljour << " " << setw(2) << setfill('0')
         << lmois << " " << lan << endl;
    jour = ljour;
    mois = lmois;
    an = lan;
}

void ex1d3() {
    /////////////////////////////////////////////////
    // Exercice I.D.3 - Nombre secret
    // ------------------------------------

    // grain de sel pour que le nombre aléatoire trouvé ne soit pas toujours le
    // même lors d'exécutions successives
    srand(time(NULL));
    int nombreAleatoire = rand() % 100;
    int nombreTentatives = 0;
    bool valeurTrouvee = false;
    cout << "Veuillez saisir un nombre compris entre 0 et 100" << endl;
    while (!valeurTrouvee) {
        string saisie;
        cin >> saisie;
        // conversion de la saisie texte en nombre entier
        int valeurSaisie = stoi(saisie);
        if (valeurSaisie == nombreAleatoire)
            valeurTrouvee = true;
        else {
            if (valeurSaisie < nombreAleatoire)
                cout << "C'est plus...";
            else
                cout << "C'est moins...";
            cout << "Nouvelle saisie : ";
        }
        nombreTentatives++;
    }
    cout << "Trouvé en " << nombreTentatives << " tentative(s)" << endl;
}

void ex1e1() {
    /////////////////////////////////////////////////
    // Exercice I.E.1 - les boucles de saisie
    // saisir une note entre 0 et 20 - fin de la saisie sur note = -1
    int note = 0;
    // tant que la note saisie est différente de -1 faire...
    while (note != -1) {
        cout << " Quelle note ? ";
        cin >> note;
        // si la note n'est pas comprise entre 0 et 20
        if (note > 20 || note < 0) {
            cout << "Note non valide !" << endl;
        }
    }
    cout << " Au revoir " << endl;
}

void ex1e2() {
    /////////////////////////////////////////////////
    // Exercice I.E.2 - la table de multiplication
    int table;

    cout << "Quelle table ? ";
    cin >> table;

    for (int i = 1; i <= 10; i++) {
        cout << table << "*" << i << "=" << table * i << endl;
    }
}

void ex1e3() {
    /////////////////////////////////////////////////
    // Exercice I.E.3 - Factorielle
    int base = 10;
    int factorielle = 1;
    for (int i = 2; i <= base; i++) {
        factorielle *= i;
    }
    cout << "Factorielle de " << base << "=" << factorielle << endl;
}

void ex1e4() {
    /////////////////////////////////////////////////
    // Exercice I.E.4 - les boucles
    // nombres premiers entre 1 et 1000 - divisible que par lui même
    int prems;
    int div, reste;

    cout << "1 n'est plus un nombre premier" << endl;
    for (prems = 2; prems <= 1000; prems++) {
        div = 1;
        do {
            div++;
            // calculer le reste de la division du nombre à qualifier de premier
            // et div
            reste = prems % div;
        }
        // tant que le reste est différent de 0 ET le diviseur est inférieur au
        // nombre à qualifier
        while ((reste != 0) && (div < prems));

        if (div == prems) cout << prems << " est un nombre premier" << endl;
    }

    int nombre = 1;
    int repete = 1000;
    int i, cpt;
    cout << "Les nombres premiers entre " << nombre << " et " << repete
         << " sont : \n";
    while (nombre <= repete) {
        cpt = 0;  // pour compter le nombre de diviseurs        nombre++;
        for (i = 1; i <= nombre; i++) {
            if ((nombre % i) == 0) {
                cpt++;
            }
        }
        if (cpt == 2)  // un nombre 1er se divise par 1 et sur lui meme, si le
                       // compteur est égal à 2, alors...        {
            cout << nombre << endl;
    }
}

void ex1f1() {
    /////////////////////////////////////////////////
    // Exercice I.F.1 - les tableaux
    // 10 entiers à saisir puis à classer dans l'ordre croissant et décroissant
    int tab[10], ordreC[10], ordreD[10];
    int min;
    cout << "trier un tableau de 10 entiers a saisir " << endl;
    for (int i = 0; i < 10; i++) {
        cout << "saisir la valeur ";
        cin >> tab[i];
    }

    // trier = tri à bulle
    for (int j = 0; j < 10; j++) {  // recherche du min à partir de l'indice 0
        min = tab[j];
        for (int i = j + 1; i < 10; i++) {
            if (tab[i] < tab[j]) {  // échanger les cases avec min
                min = tab[i];
                tab[i] = tab[j];
                tab[j] = min;
            }
        }
        ordreC[j] = min;      //  croissant : min à partir de 0
        ordreD[9 - j] = min;  // décroissant min à partir du max
    }
    cout << "ordre croissant : ";
    for (int i = 0; i < 10; i++) cout << ordreC[i] << " ";
    cout << endl;

    cout << "ordre decroissant : ";
    for (int i = 0; i < 10; i++) cout << ordreD[i] << " ";
    cout << endl;
}

void ex1g1() {
    /////////////////////////////////////////////////
    // Exercice I.G.1 - les chaînes de caractères
    string motSaisi;
    cout << "Saisir le mot a passer en majuscule : ";
    cin >> motSaisi;
    cout << "Mot saisi   : " << motSaisi << endl;
    cout << "Mot en majuscule : ";
    for (int i = 0; i < motSaisi.length(); i++) {
        char carChiffre = motSaisi[i] - 32;
        if (carChiffre < 65) {
            carChiffre += 65;
        }
        cout << carChiffre;
    }
    cout << endl;
}

void ex1g2() {
    /////////////////////////////////////////////////
    // Exercice I.G.2 - Cryptographie de César
    // -------------------------------------
    // 1ère façon
    string motSaisi;
    cout << "Saisir le mot a chiffrer : ";
    cin >> motSaisi;
    cout << "Mot saisi   : " << motSaisi << endl;
    cout << "Mot chiffre : ";
    for (int i = 0; i < motSaisi.length(); i++) {
        char carChiffre = (motSaisi[i] + 3) % 123;
        if (carChiffre < 97) {
            carChiffre += 97;
        }
        cout << carChiffre;
    }
    cout << endl;

    // 2ème façon
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    const int taille = 26;
    char encodage[taille];
    cout << "Quel decalage ? = ";
    int decalage;
    cin >> decalage;
    for (int i = 0; i < taille; i++) {
        encodage[i] = alphabet[(i + decalage) % taille];
    }
    cout << "Saisir le mot a chiffrer : ";
    cin >> motSaisi;
    cout << "Mot saisi   : " << motSaisi << endl;
    cout << "Mot chiffre : ";
    for (int i = 0; i < motSaisi.length(); i++) {
        // trouver la position dans l'aphabet de la lettre
        int pos = alphabet.find(motSaisi[i]);
        // afficher sa correspondance à partir de la table d'encodage
        cout << encodage[pos];
    }
    cout << endl;
}

void ex2_1() {
    /////////////////////////////////////////////////
    // Exercice II.1 - ASCII art
    int n, espace, etoiles;

    cout << "Entrez la hauteur du sapin : ";
    cin >> n;

    // Boucle pour dessiner chaque ligne du sapin
    for (int i = 1; i <= n; i++) {
        espace = n - i;
        etoiles = 2 * i - 1;

        // Afficher les espaces à gauche
        for (int j = 0; j < espace; j++) {
            cout << " ";
        }

        // Afficher les étoiles
        for (int k = 0; k < etoiles; k++) {
            cout << "*";
        }

        // Aller à la ligne pour la prochaine ligne du sapin
        cout << "\n";
    }

    // Afficher le tronc du sapin
    int tronc = n / 3;
    for (int i = 0; i < tronc; i++) {
        for (int j = 0; j < n - 2; j++) {
            cout << " ";
        }
        cout << "| |\n";
    }
}

void ex2_2() {
    /////////////////////////////////////////////////
    // Exercice II.2 - Guirlande
    const int nbAmpoules = 10;
    bool ampoules[nbAmpoules] = {
        false};  // Initialiser toutes les ampoules éteintes
    int choix;

    while (1) {
        // Afficher la guirlande
        for (int i = 0; i < nbAmpoules; i++) {
            if (ampoules[i]) {
                printf("*");
            } else {
                printf("-");
            }
        }
        printf("\n");

        // Allumer ou éteindre chaque deuxième ampoule successivement
        for (int i = 1; i < nbAmpoules; i += 2) {
            ampoules[i] = !ampoules[i];  // Inverser l'état de l'ampoule
        }

        // Attendre quelques millisecondes pour visualiser les changements
        Sleep(200);  // Attendre 200 ms

        // Effacer la console
        system("cls");  // Pour Windows

        // Attention, boucle infinie !
    }
}

int main() {
    // ex1a();
    // ex1b();
    //ex1c();
    //ex1d1();
    //ex1d2();
    //ex1d3();
    //ex1e1();
    //ex1e2();
    //ex1e3();
    // ex1f();
    //ex1g1();
    //ex1g2();
    ex2_1();
    // ex2_2();

}  // fin du programme
