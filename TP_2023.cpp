// 2SNTP1.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme
// commence et se termine à cet endroit.
//
using namespace std;

#include <iomanip>
#include <iostream>
#include <string>

void ex1a() {
    int a, b;
    a = 3;
    b = 5;
    a = b;
    b = a;
    // Le programme n'affiche rien (pas de directive cout).
}

void ex1b() {
    float a, b;  // : réels
    int c;       // : entier

    a = 2.3f;
    b = 3.5f;
    c = a + b;
    cout << "c = " << c << endl;
    // c vaut 5
}

void ex1c() {
    /*
    -6 à 7 ans : poussin
    - 8 à 9 ans : pupille
    - 10 à 11 ans : minime
    - 12 ans et plus : cadet
 */

    int age;
    cout << "Quel est votre age ? ";
    cin >> age;

    // Si l'âge est compris entre 6 et 7 inclus alors...
    if (age >= 6 && age <= 7) {
        // afficher la chaîne ci-dessous
        cout << "tu es poussin" << endl;
    }
    if (age >= 8 && age <= 9) {
        cout << "tu es pupille" << endl;
    }
    if (age >= 10 && age <= 11) {
        cout << "tu es minime" << endl;
    }
    if (age >= 12) cout << "tu es cadet" << endl;
}

void ex1d() {
    /////////////////////////////////////////////////
    // Exercice D - les conditions
    // Note : années bissextiles non prises en charge
    // donner le lendemain du jour saisi
    int jour, mois, an;
    int ljour, lmois, lan;

    cout << "Saisir le jour le mois l'annee svp (separes par des espaces ou "
            "RETOUR CHARIOT) ";  // carriage return
    cin >> jour >> mois >> an;
    cout << "Le lendemain du " << jour << " " << mois << " " << an << endl;
    // on passe au lendemain
    ljour = jour + 1;

    lmois = mois;
    lan = an;

    // fin du mois
    // mois 31 jours - 01 03 05 07 08 10 12
    // mois 30 jours - 04 06 09 11
    // février - 28
    // cas fin mois 31
    if ((ljour == 32) && (mois == 1 || mois == 3 || mois == 5 || mois == 7 ||
                          mois == 8 || mois == 10 || mois == 12)) {
        ljour = 1;
        // % : modulo
        // ça remet le mois à 1 => quand la saisie correspond au 31/12 le mois
        // passe à Janvier
        lmois = (lmois + 1) % 12;
        if (lmois == 1) lan++;  // cas 31 déc
    }
    // cas fin mois 30
    if ((ljour == 31) && (mois == 4 || mois == 6 || mois == 9 || mois == 11)) {
        ljour = 1;
        lmois++;
    }

    // cas février
    if ((ljour == 29) && (lmois == 2)) {
        ljour = 1;
        lmois++;
    }

    cout << setw(2) << setfill('0') << ljour << " " << setw(2) << setfill('0')
         << lmois << " " << lan << endl;
    jour = ljour;
    mois = lmois;
    an = lan;
}

void ex1e() {
    /////////////////////////////////////////////////
    // Exercice E - les boucles
    // saisir une note entre 0 et 20 - fin de la saisie sur note = -1
    int note = 0;
    // tant que la note saisie est différente de -1 faire...
    while (note != -1) {
        cout << " Quelle note ? ";
        cin >> note;
        // si la note n'est pas comprise entre 0 et 20
        if (note > 20 || note < 0) {
            cout << "Note non valide !" << endl;
        }
    }
    cout << " Au revoir " << endl;
}

void ex1f() {
    /////////////////////////////////////////////////
    // Exercice F - les boucles
    // nombres premiers entre 1 et 1000 - divisible que par lui même
    int prems;
    int div, reste;

    cout << "1 n'est plus un nombre premier" << endl;
    for (prems = 2; prems <= 1000; prems++) {
        div = 1;
        do {
            div++;
            // calculer le reste de la division du nombre à qualifier de premier
            // et div
            reste = prems % div;
        }
        // tant que le reste est différent de 0 ET le diviseur est inférieur au
        // nombre à qualifier
        while ((reste != 0) && (div < prems));

        if (div == prems) cout << prems << " est un nombre premier" << endl;
    }

    int nombre = 1;
    int repete = 1000;
    int i, cpt;
    cout << "Les nombres premiers entre " << nombre << " et " << repete
         << " sont : \n";
    while (nombre <= repete) {
        cpt = 0;  // pour compter le nombre de diviseurs        nombre++;
        for (i = 1; i <= nombre; i++) {
            if ((nombre % i) == 0) {
                cpt++;
            }
        }
        if (cpt == 2)  // un nombre 1er se divise par 1 et sur lui meme, si le
                       // compteur est égale à 2, alors...        {
            cout << nombre << endl;
    }
}

void ex1g() {
    /////////////////////////////////////////////////
    // Exercice G - les boucles
    // la table de multiplication
    int table;

    cout << "Quelle table ? ";
    cin >> table;

    for (int i = 1; i <= 10; i++) {
        cout << table << "*" << i << "=" << table * i << endl;
    }
}

void ex1h() {
    /////////////////////////////////////////////////
    // Exercice H - les tableaux
    // 10 entiers à saisir puis à classer dans l'ordre croissant et décroissant
    int tab[10], ordreC[10], ordreD[10];
    int min;
    cout << "trier un tableau de 10 entiers a saisir " << endl;
    for (int i = 0; i < 10; i++) {
        cout << "saisir la valeur ";
        cin >> tab[i];
    }

    // trier = tri à bulle
    for (int j = 0; j < 10; j++) {  // recherche du min à partir de l'indice 0
        min = tab[j];
        for (int i = j + 1; i < 10; i++) {
            if (tab[i] < tab[j]) {  // échanger les cases avec min
                min = tab[i];
                tab[i] = tab[j];
                tab[j] = min;
            }
        }
        ordreC[j] = min;      //  croissant : min à partir de 0
        ordreD[9 - j] = min;  // décroissant min à partir du max
    }
    cout << "ordre croissant : ";
    for (int i = 0; i < 10; i++) cout << ordreC[i] << " ";
    cout << endl;

    cout << "ordre decroissant : ";
    for (int i = 0; i < 10; i++) cout << ordreD[i] << " ";
    cout << endl;
}

void ex2c() {
    // ------------------------------------
    // Exercice 2.c : Compagnie d'assurance
    // ------------------------------------
    // Variables age, perm, acc, assur, p en Numérique
    int age, perm, acc, assur, p;
    // Variables C1, C2, C3 en Booléen
    bool c1, c2, c3;
    // Variable situ en Chaîne de caractère
    string situ;
    // pour boucler
    int sortie = 0;
    int nombre = 1;
    do {
        cout << "Candidat n:" << nombre << endl;
        // Début
        // Ecrire "Entrez l'âge: "
        cout << "Entrez l'age : ";
        // Lire age
        cin >> age;
        // Ecrire "Entrez le nombre d'années de permis: "
        cout << "Entrez le nombre d'annees de permis : ";
        // Lire perm
        cin >> perm;
        // Ecrire "Entrez le nombre d'accidents: "
        cout << "Entrez le nombre d'accidents : ";
        // Lire acc
        cin >> acc;
        // Ecrire "Entrez le nombre d'années d'assurance: "
        cout << "Entrez le nombre d'annees d'assurance : ";
        // Lire assur
        cin >> assur;
        // C1 ← age >= 25
        c1 = age >= 25;
        // C2 ← perm >= 2
        c2 = perm >= 2;
        // C3 ← assur > 5
        c3 = assur > 5;
        // P ← 0
        p = 0;
        // Si Non(C1) Alors
        //   P ← P + 1
        // FinSi
        if (!c1) {
            p++;
        }
        // Si Non(C2) Alors
        //   P ← P + 1
        // FinSi
        if (!c2) {
            p++;
        }
        // P ← P + acc
        p += acc;
        // Si P < 3 et C3 Alors
        //   P ← P - 1
        // FinSi
        if (p < 3 && c3) {
            p--;
        }
        // Si P = -1 Alors
        //   situ ← "Bleu"
        if (p == -1) {
            situ = "Bleu";
        }
        // SinonSi P = 0 Alors
        //   situ ← "Vert"
        else if (p == 0) {
            situ = "Vert";
        }
        // SinonSi P = 1 Alors
        //   situ ← "Orange"
        else if (p == 1) {
            situ = "Orange";
        }
        // SinonSi P = 2 Alors
        //   situ ← "Rouge"
        else if (p == 2) {
            situ = "Rouge";
        }
        // Sinon
        //   situ ← "Refusé"
        // FinSi
        else {
            situ = "Refuse";
        }
        // Ecrire "Votre situation : ", situ
        cout << "Votre situation : " << situ << endl;
        // continuer ?
        cout << "Continuer la saisie ? (-1 pour quitter, 0 pour continuer):";
        cin >> sortie;
        nombre++;
        // Fin
    } while (sortie != -1);
}

void ex3() {
    // ------------------------------------
    // Exercice 3 : Nombre secret
    // ------------------------------------

    // grain de sel pour que le nombre aléatoire trouvé ne soit pas toujours le
    // même lors d'exécutions successives
    srand(time(NULL));
    int nombreAleatoire = rand() % 100;
    int nombreTentatives = 0;
    bool valeurTrouvee = false;
    cout << "Veuillez saisir un nombre compris entre 0 et 100" << endl;
    while (!valeurTrouvee) {
        string saisie;
        cin >> saisie;
        // conversion de la saisie texte en nombre entier
        int valeurSaisie = stoi(saisie);
        if (valeurSaisie == nombreAleatoire)
            valeurTrouvee = true;
        else {
            if (valeurSaisie < nombreAleatoire)
                cout << "C'est plus...";
            else
                cout << "C'est moins...";
            cout << "Nouvelle saisie : ";
        }
        nombreTentatives++;
    }
    cout << "Trouvé en " << nombreTentatives << " tentative(s)" << endl;
}

void ex4b() {
    // ------------------------------------
    // Exercice 4.b : Médiane
    // ------------------------------------

    // taille arbitraire pour l'exemple
    const int n = 11;
    float temp[n] = {-87.4, 30.0, 54.4, -14.4, 87.8, 44.4,
                     98.4,  41.0, 11.5, 41.1,  66.9};
    // reprise de l'algo de tri de l'exercice 1.h
    float min;
    // trier = tri à bulle
    for (int j = 0; j < n; j++) {  // recherche du min à partir de l'indice 0
        min = temp[j];
        for (int i = j + 1; i < n; i++) {
            if (temp[i] < temp[j]) {  // échanger les cases avec min
                min = temp[i];
                temp[i] = temp[j];
                temp[j] = min;
            }
        }
    }
    cout << "ordre croissant : ";
    for (int i = 0; i < n; i++) cout << temp[i] << " ";
    cout << endl;
    // afficher la médiane => l'élément au milieu du tableau
    int indiceMilieu = n / 2;
    cout << "Médiane = " << temp[indiceMilieu] << endl;
}

void ex4c() {
    // -------------------------------------
    // Exercice 4.c : Cryptographie de César
    // -------------------------------------
    // 1ère façon
    string motSaisi;
    cout << "Saisir le mot a chiffrer : ";
    cin >> motSaisi;
    cout << "Mot saisi   : " << motSaisi << endl;
    cout << "Mot chiffre : ";
    for (int i = 0; i < motSaisi.length(); i++) {
        char carChiffre = (motSaisi[i] + 3) % 123;
        if (carChiffre < 97) {
            carChiffre += 97;
        }
        cout << carChiffre;
    }
    cout << endl;

    // 2ème façon
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    const int taille = 26;
    char encodage[taille];
    cout << "Quel decalage ? = ";
    int decalage;
    cin >> decalage;
    for (int i = 0; i < taille; i++) {
        encodage[i] = alphabet[(i + decalage) % taille];
    }
    cout << "Saisir le mot a chiffrer : ";
    cin >> motSaisi;
    cout << "Mot saisi   : " << motSaisi << endl;
    cout << "Mot chiffre : ";
    for (int i = 0; i < motSaisi.length(); i++) {
        // trouver la position dans l'aphabet de la lettre
        int pos = alphabet.find(motSaisi[i]);
        // afficher sa correspondance à partir de la table d'encodage
        cout << encodage[pos];
    }
    cout << endl;
}
int main() {
    // ex1a();
    // ex1b();
    // ex1c();
     // ex1d();
    //ex1e();
    //ex1f();
   //ex1g();
    ex1h();
    // ex2c();
    // ex3();
    // ex4b();
    // ex4c();
    

}  // fin du programme
